resource "hcloud_server" "master" {

  depends_on = [
    hcloud_network_subnet.kube_subnet,
  ]

  name        = var.master_name
  server_type = var.master_type
  image       = var.image
  location    = var.location
  user_data   = templatefile("files/master_config.yml",{
    key = hcloud_ssh_key.key.public_key
  })
}

resource "hcloud_server_network" "master" {
  server_id   = hcloud_server.master.id
  network_id  = hcloud_network.kube.id
  ip          = var.master_ip
}

resource "null_resource" "master" {
  depends_on = [
    hcloud_server.master,
    hcloud_server_network.master,
  ]
}
