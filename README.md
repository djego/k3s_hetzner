k3s Hetzner
===========

How to build a kubernetes cluster in [Hetzner Cloud](https://www.hetzner.com/cloud).

## Why

Hetzner is a good cloud provider in Europe, this project show us how build a kubernetes cluster in Hetzner Cloud, using [k3s](https://k3s.io/), [cloud-init](https://cloudinit.readthedocs.io/en/latest/), [Terraform](https://www.terraform.io/) and [Ansible](https://docs.ansible.com/ansible/latest/index.html).

## Requirements.

- A Hetzner Cloud account.
- k3s 0.10.
- Terraform 0.12.
- Ansible 2.8.
